const express = require("express");
const recordRoutes = express.Router()
const dbo = require("../db/conn")
const ObjectId = require("mongodb").ObjectId

// SHOW DB WITH FILTER AND SORT =>
recordRoutes.route("/products").get(function(req, res) {
    let db_connect = dbo.getDb("products")
    let myQuery = {}
    let sort = {}

    const collectionKeys = ['nazwa', 'cena', 'opis', 'ilosc', 'jednostka_miary']
    collectionKeys.forEach(key => {
        if (req.query[key]){
            if (key === 'cena') {
                myQuery[key] = parseFloat(req.query[key])
            } else if (key === 'ilosc'){
                myQuery[key] = parseInt(req.query[key])
            } else {
                myQuery[key] = req.query[key]
            }
        }
    })

    if(req.query.sortBy) {
        sort[req.body.sortBy] = ( req.query.sortOrder ==  1) ? 1 : -1;
    }

    db_connect.collection("products").find(myQuery).sort(sort).toArray(function(err, result){
        if(err) throw err;
        res.json(result);
    })
})

// ADD TO DB =>
recordRoutes.route("/products").post(function(req, response) {
    let db_connect = dbo.getDb();
    let myObj = {
        nazwa: req.body.nazwa,
        cena: req.body.cena,
        opis: req.body.opis,
        ilosc: req.body.ilosc,
        jednostka_miary: req.body.jednostka_miary
    };
        db_connect.collection("products").findOne({nazwa: req.body.nazwa}, function(err, foundDocument) {
            if(err) throw err;
            if (foundDocument) {
                response.status(400).json({Error: "Name is not unique"})
            } else {
                db_connect.collection("products").insertOne(myObj, function(err, res) {
                    if(err) throw err;
                    response.json(res);
                })
            }
        })
} )
// FIND BY ID =>
recordRoutes.route("/products/:id").get(function(req, res) {
    let db_connect = dbo.getDb("products")
    let myQuery = {_id: ObjectId(req.params.id)};

    db_connect.collection("products").findOne(myQuery, function(err, result) {
        if(err) throw err;
        res.json(result);
    })
})
// UPDATE BY ID =>
recordRoutes.route("/products/:id").put(function(req, response){
    let db_connect = dbo.getDb("products");
    let myQuery = {_id: ObjectId(req.params.id)};
    let newValues = req.body
    let setNewValues = {
        $set: newValues
    }
    
    db_connect.collection("products").updateOne(myQuery, setNewValues, function(err, res){
        if(err) throw err;
        console.log("1 document updated successfully")
        response.json(res)
    })
})
// DELETE FROM DB =>
recordRoutes.route("/products/:id").delete(function(req, response) {
    let db_connect = dbo.getDb("products");
    let myQuery = {_id: ObjectId(req.params.id)};
    db_connect.collection("products").findOne(myQuery, function(err, foundDocument) {
        if(err) throw err;
        if (foundDocument) {
            db_connect.collection("products").deleteOne(myQuery, function(err, res) {
                if(err) throw err;
                response.json(res)
            })
        } else {
            response.status(400).json({Error: "No such product!"})
        }
    })
})

recordRoutes.route("/products/user/generateReport").get(function(req, response){
    let db_connect = dbo.getDb("products")
    db_connect.collection("products").aggregate([
        { $project: {"_id": 0, "name": "$nazwa", "amount": "$ilosc", "totalValue": {$round: [{ $multiply: ["$cena", "$ilosc"] }, 2]} } }
    ]).toArray(function(err, res) {
        if(err) throw err;
        response.json(res)
    })
})

module.exports = recordRoutes;