// Selecting the products database to use.
use('products');

// Inserting a few documents into the products collection
db.getCollection('products').insertMany([
    {
      "nazwa": "Kakao",
      "cena": 30.99,
      "opis": "Ciemne kakao",
      "ilość": 50,
      "jednostka_miary": "op."
    },
    {
      "nazwa": "Makaron",
      "cena": 10.50,
      "opis": "Makaron spaghetti",
      "ilość": 30,
      "jednostka_miary": "op."
    },
    {
      "nazwa": "Jablka",
      "cena": 2.99,
      "opis": "Jablka z polskich sadow",
      "ilość": 20,
      "jednostka_miary": "kg"
    },
    {
      "nazwa": "Ziemniaki",
      "cena": 1.99,
      "opis": "Znane rowniez jako pyry i kartofle",
      "ilość": 15,
      "jednostka_miary": "kg"
    },
    {
      "nazwa": "Cebula",
      "cena": 0.49,
      "opis": "Najbardziej polskie warzywo",
      "ilość": 999,
      "jednostka_miary": "kg"
    }
]);
